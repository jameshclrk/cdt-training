#!/usr/bin/env python3

import pandas
import matplotlib.pyplot as plt
import numpy as np

omp = pandas.read_csv('stream_output.txt', names=['cores', 'mbs'], delimiter=' ')

ax = plt.subplot(1,1,1)
omp.plot.line(x='cores',y='mbs',ax=ax,label='stream')
ax.set_ylabel('MB/s')

ax.set_xlim(1,64)

plt.savefig("scaling.png")
plt.show()
