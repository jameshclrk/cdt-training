#!/usr/bin/env python3

import pandas
import matplotlib.pyplot as plt
import numpy as np

omp = pandas.read_csv('jacobi_output.txt', names=['cores', 'time'], delimiter=' ')

timelist = omp['time'].tolist()
baseline = timelist[0]
speedup = [ float(baseline)/float(x) for x in timelist]

ompScaling = pandas.DataFrame({'cores':omp['cores'].tolist(),'Speedup':speedup})
ompIdeal = pandas.DataFrame({'cores':omp['cores'].tolist(),'Speedup':omp['cores'].tolist()})

ax = plt.subplot(1,1,1)
ompScaling.plot.line(x='cores',y='Speedup',ax=ax,label='JacobiOMP')
ompIdeal.plot.line(x='cores',y='Speedup',ax=ax,label='Ideal')
ax.set_ylabel('Speedup')


plt.savefig("scaling.png")
plt.show()
