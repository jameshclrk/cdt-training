#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

  int nthreads, tid;

/* Fork a team of threads */
#pragma omp parallel private(nthreads, tid)
  {
    tid = omp_get_thread_num();
    printf("Hello from thread = %d\n", tid);
    nthreads = omp_get_num_threads();
    printf("#threads = %d\n", nthreads);
  }
}
