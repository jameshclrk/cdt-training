#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main()

/*
  Purpose:
    MAIN is the main program for JACOBI_MPI.
  Discussion:
    JACOBI_MPI carries out a Jacobi iteration with MPI.
  Licensing:
    This code is distributed under the GNU LGPL license.
  Modified:
    19 February 2020
  Author:
    John Burkardt
    Modified STFC
*/

{
  double *input;
  double diff;
  int i;
  int it;
  int const n_iterations = 5000;
  int const n_variables = 480000;
  double residual;
  double t;
  double *solution;
  double *global_solution;
  double *iterative_solution;

  // Initialise the MPI Library;
  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_SINGLE, &provided);

  // TODO: MPI - WORK OUT LOCAL RANK AND NUMBER OF RANKS
  int rank = 0;
  int num_ranks = 1;

  if (rank == 0) {
    printf("\n");
    printf("JACOBI_MPI:\n");
    printf("  C/MPI version\n");
    printf("  Jacobi iteration to solve A*x=b.\n");
    printf("\n");
    printf("  Number of variables  N = %d\n", n_variables);
    printf("  Number of iterations M = %d\n", n_iterations);

    printf("\n");
    printf("  IT     l2(dX)    l2(resid)\n");
    printf("\n");
  }

  // TODO: MPI - WHICH VARIABLES NEED TO BE COMPUTED LOCALLY (BONUS QUESTION:
  // THIS SHOULD WORK FOR ANY NUMBER OF RANKS)
  int const local_variables = n_variables;
  int const start_variable = 0;

  input = (double *)malloc(local_variables * sizeof(double));
  solution = (double *)malloc(local_variables * sizeof(double));
  iterative_solution = (double *)malloc(local_variables * sizeof(double));

  // Set up the right hand side.
  for (i = 0; i < local_variables; i++) {
    input[i] = 0.0;
  }

  // Finish initialisation of the input in the last slot of the array
  if (rank == num_ranks - 1) {
    input[local_variables - 1] = (double)(n_variables + 1);
  }

  // Initialize the solution estimate to 0.
  // Exact solution is (1,2,3,...,N).
  for (i = 0; i < n_variables; i++) {
    solution[i] = 0.0;
  }

  // TODO: - MPI. SEND INITIAL HALO VALUES TO NEIGHBOURING RANKS (SOLUTION).
  double halo_low = 0.0;
  double halo_high = 0.0;

  // Iterate M times.
  for (it = 0; it < n_iterations; it++) {

    // Jacobi update.
    for (i = 0; i < n_variables; i++) {
      iterative_solution[i] = input[i];
      if (0 < i) {
        iterative_solution[i] = iterative_solution[i] + solution[i - 1];
      }
      // TODO: - MPI. IF WE HAVE LOW HALO THEN USE IT HERE.
      // else if(...){
      // iterative_solution[i] = iterative_solution[i] + low_halo;
      //}
      if (i < n_variables - 1) {
        iterative_solution[i] = iterative_solution[i] + solution[i + 1];
      }
      // TODO: - MPI. IF WE HAVE HIGH HALO THEN USE IT HERE.
      // else if(...){
      // iterative_solution[i] = iterative_solution[i] + halo_high;
      //}
      iterative_solution[i] = iterative_solution[i] / 2.0;
    }

    // Difference.
    diff = 0.0;
    for (i = 0; i < n_variables; i++) {
      diff = diff + pow(solution[i] - iterative_solution[i], 2);
    }

    // Overwrite old solution.
    for (i = 0; i < n_variables; i++) {
      solution[i] = iterative_solution[i];
    }

    // TODO: - MPI. UPDATE THE HALO VALUES WITH THE LATEST SOLUTION

    // Residual.
    residual = 0.0;
    for (i = 0; i < n_variables; i++) {
      t = input[i] - 2.0 * solution[i];
      if (0 < i) {
        t = t + solution[i - 1];
      }
      // TODO: - MPI. IF WE HAVE LOW HALO THEN USE IT HERE.
      // else if(...){
      // t = t + low_halo;
      //}
      if (i < n_variables - 1) {
        t = t + solution[i + 1];
      }
      // TODO: - MPI. IF WE HAVE HIGH HALO THEN USE IT HERE.
      // else if(...){
      // t = t + halo_high;
      //}
      residual = residual + t * t;
    }

    if ((it < 10 || n_iterations - 10 < it)) {
      double temp_residual = residual, temp_difference = difference;
      // TODO: - MPI. COMPUTE THE GLOBAL RESIDUAL AND DIFFERENCE TO OUTPUT.
      if (rank == 0) {
        printf("  %8d  %14.6g  %14.6g\n", it, sqrt(temp_difference),
               sqrt(temp_residual));
      }
    }
    if (rank == 0 && it == 9) {
      printf("  Omitting intermediate results.\n");
    }
  }

  // Write part of final estimate.
  printf("\n");
  printf("  Part of final solution estimate:\n");
  printf("\n");
  global_solution = (double *)malloc(n_variables * sizeof(double));
  // TODO: - MPI. GATHER THE RESULT ON NODE 0
  for (i = 0; i < 10; i++) {
    printf("  %8d  %14.6g\n", i, global_solution[i]);
  }
  printf("...\n");
  for (i = n_variables - 11; i < n_variables; i++) {
    printf("  %8d  %14.6g\n", i, global_solution[i]);
  }

  // Free memory.
  free(input);
  free(solution);
  free(iterative_solution);
  free(global_solution);

  // Terminate.
  printf("\n");
  printf("JACOBI_MPI:\n");
  printf("  Normal end of execution.\n");

  return 0;
}
