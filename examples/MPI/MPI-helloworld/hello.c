#include <mpi.h>
#include <stdio.h>
int main(int *argc, char **argv) {
  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_SINGLE, &provided);
  if (provided < MPI_THREAD_SINGLE)
    return 1;
  int world_size, world_rank;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  printf("Hello from node %i of %i\n", world_rank, world_size);
  MPI_Finalize();
  return 0;
}
