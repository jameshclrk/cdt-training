#include <stdlib.h>
#include <stdio.h>
#include <omp.h>


#define SIZE 100000000
#define INDEXES 8 
#define STRIP_WIDTH 32
#define PADDED_SIZE (SIZE/STRIP_WIDTH)*STRIP_WIDTH

void initialise(int *hist, int *b, int *c){

srand(1);
for(int i = 0; i < SIZE; i++){
  int index1 = rand() % INDEXES;
  int index2 = rand() % INDEXES;
  b[i] = index1;
  c[i] = index2;
}
for(int i = 0; i < INDEXES*2; i++){
  hist[i] = 0;
}

}

int main(){

/* We pad the arrays. */
int histogram[INDEXES*2], *b, *c;
int strip_index[STRIP_WIDTH];
double start,finish;

b = _mm_malloc(sizeof(int)*PADDED_SIZE, 64);
c = _mm_malloc(sizeof(int)*PADDED_SIZE, 64);

initialise(histogram, b, c);

/* Compute a histogram - e.g. histogram analysis of an image. */
/* We strip mine the loop, allowing vectorisation */
start = omp_get_wtime();
for(int i = 0; i < SIZE; i+= STRIP_WIDTH){

    for(int j = 0; j < STRIP_WIDTH; j++){
      int index = b[j+i] + c[j+i];  
      strip_index[j] = index;
    }

    for(int j = 0; j < STRIP_WIDTH; j++){
      histogram[strip_index[j]]++;
    }
}
finish = omp_get_wtime();
printf("Runtime was %lfs.\n", finish-start);


for(int i = 0; i < INDEXES*2; i++){
  printf("%i ", histogram[i]);
}
printf("\n");

_mm_free(b);
_mm_free(c);

}

