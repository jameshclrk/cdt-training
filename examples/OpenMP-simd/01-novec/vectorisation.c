#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define SIZE 16000000

int main (void) {
    double start, end;
    double _a, _b, _c;
    double * a, * b, *c;
    double * a_vec, * b_vec, *c_vec;

    a = _mm_malloc(SIZE * sizeof(double), 64);
    b = _mm_malloc(SIZE * sizeof(double), 64);
    c = _mm_malloc(SIZE * sizeof(double), 64);
    a_vec = _mm_malloc(SIZE * sizeof(double), 64);
    b_vec = _mm_malloc(SIZE * sizeof(double), 64);
    c_vec = _mm_malloc(SIZE * sizeof(double), 64);

    start = omp_get_wtime();
    for (int i = 0; i < SIZE; i++) {
        _a = (double)rand() / (double)RAND_MAX;;
        _b = (double)rand() / (double)RAND_MAX;;
        _c = (double)rand() / (double)RAND_MAX;;

        a[i] = _a;
        b[i] = _b;
        c[i] = _c;
        a_vec[i] = _a;
        b_vec[i] = _b;
        c_vec[i] = _c;
        
    }
    end = omp_get_wtime();
    printf("Initialisation:\t\t%lfs\n", end-start);

    start = omp_get_wtime();
#pragma novector
    for (int i = 0; i < SIZE; i++) {
        a[i] += b[i] * c[i];
    }
    end = omp_get_wtime();
    printf("\"novector\" loop took:\t%lfs\n", end-start);

    start = omp_get_wtime();
    for (int i = 0; i < SIZE; i++) {
        a_vec[i] += b_vec[i] * c_vec[i];
    }
    end = omp_get_wtime();
    printf("Vectorised loop took:\t%lfs\n", end-start);
    return 0;
}
