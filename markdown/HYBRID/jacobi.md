# Hybrid Exercises

## Setup
The initial code for these exercises is in the `examples/Hybrid`. This exercise will look at the Jacobi solver you have seen, but this time you will need to add both OpenMP and MPI.

To compile this exercices load the appropriate environment with `module load cdt-training/intel_tools`.

## Jacobi Example

In this exercise you will use MPI and OpenMP to parallelise the Jacobi algorithm implementation. Go to `examples/Hybrid/Hybrid-Jacobi`.
The code functionality is identical to the code you used during the OpenMP and MPI lectures.

Remember to use the reservation with `#BSUB -U intel_skl@scafell-pike`.

- First of all, submit a job to run it serially and save the time and results obtained.

- The code isn't commented with hints, but you can use your previous implementations to guide your hybrid implementation.

- Compile your code, using `mpiicc` and adding the `-fopenmp` flag.

- Look at the submission script in detail. The initial script submits 2 ranks (`#BSUB -n 2`) with 2 ranks per physical node (`#BSUB -R 'span[ptile=2]'`), and
  16 OpenMP threads per rank. Submit the job, and record the performance. Try altering the number of ranks and OpenMP threads and see what gives best performance 
  (keep the total number of threads to 32).

- Try altering the total number of threads to 64 using two physical nodes (So `#BSUB -n 2*X` for `#BSUB -R 'span[ptile=X]'`). How does the performance compare?

- Make sure you still get the same results. Has the execution time improved? What is the speedup? Which setup of MPI ranks and OpenMP threads performs best?
