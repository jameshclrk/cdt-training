
mkdir -p build
pandoc CUDA/cuda.md -f markdown -t rst -s -o source/CUDA.rst
pandoc OpenMP-simd/draft.md  -f markdown -t rst -s -o source/OpenMP-simd.rst
pandoc OpenMP-worksharing/draft.md  -f markdown -t rst -s -o source/OpenMP-worksharing.rst
pandoc MPI/jacobi.md -f markdown -t rst -s -o source/MPI-examples.rst
pandoc HYBRID/jacobi.md -f markdown -t rst -s -o source/Hybrid-example.rst
pandoc login.md -f markdown -t rst -s -o source/login.rst

cp -r ../presentations/* source/.

make latex
cd build/latex
sed -i 's/PDFLATEX = latexmk -pdf -dvi- -ps-/PDFLATEX = tectonic/g' Makefile
make
cp OpenMPTraining.pdf ../../source/CDTTraining.pdf
cd ../../

make html

rm source/OpenMP-simd.rst
rm source/OpenMP-worksharing.rst
rm source/login.rst
rm source/*.pdf
