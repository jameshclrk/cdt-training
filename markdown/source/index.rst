Hartree Centre CDT Training Materials and Exercises
======================================================

Introduction
------------
Welcome to the Hartree Centre CDT Training course. In this course you will learn to create parallel programs using CUDA, MPI and OpenMP.

Lecturers
---------
- Vassil Alexandrov (Introduction to Parallel Computing)
- Jony Castagna (CUDA) 
- Aiman Shaikh (CUDA)
- Sergi Siso (OpenMP)
- Jack Taylor (OpenMP)
- Aidan Chalk (MPI)
- James Clark (MPI)
- Simon Goodchild (Big Data Analytics)

Lecture Materials
-----------------


**Materials for the course:**

- Lecture 1 is an introduction to parallel computing, and the slides are available here: :download:`OpenMP Worksharing constructs <./OpenMP-worksharing-training.pdf>`.

- Lecture 2 is an :download:`Introduction to CUDA <./OpenMP-simd-training.pdf>`. The lecture goes through ......

- Lecture 3 is an :download:`Introduction to OpenMP <./OpenMP-task-training.pdf>`. This introduces you to the basics of OpenMP, such as OpenMP :code:`for` constructs, as well as the data sharing clauses commonly used in OpenMP programs, such as :code:`shared` and :code:`private`.

- Lecture 4 is an :download:`Introduction to MPI <./NumericalLibraries.pdf>`. This lecture covers the basics of MPI, including Point to Point communication (:code:`MPI_Send`/:code:`MPI_Recv`) and a variety of commonly used collectives.

- Lecture 5 gives :download:`Tips and Tricks for Hybrid Parallelism <./whoKnows.pdf>`. In this lecture we discuss Hybrid Memory parallelism, which mixes MPI and OpenMP/CUDA to enable computation on modern HPC systems.

- Lecture 6 ....?

Exercises
---------

Use `git clone https://gitlab.com/hartreetraining/cdt-training.git` to download the exercises after :doc:`logging into Scafell Pike.</login>`

A pdf version of this webpage is also available :download:`here <./CDTTraining.pdf>`.


Table of Contents
-----------------

.. toctree::
   :maxdepth: 1

   Home<index.rst>
   login.rst
   CUDA.rst
   OpenMP-worksharing.rst
   OpenMP-simd.rst
   MPI-examples.rst
   Hybrid-example.rst
