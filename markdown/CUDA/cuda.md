# CUDA Examples

## Setup
The exercises and material for the CUDA course are available on the NVidia Deep Learning Institute (NVidia DLI) webpage after registration [here](https://developer.nvidia.com/) and the Access Code released on the training day. The CUDA course is [here](https://courses.nvidia.com/courses/course-v1:DLI+C-AC-01+V1/about/)



