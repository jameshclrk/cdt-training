Machine Access
==============

## Using Scafell Pike
Connect to the machine with `ssh -YC username@hcxlogin1.hartree.stfc.ac.uk`.

Access the primary filesystem with `cd $HCBASE`. You can load all the modules you will need with `module load cdt-training/intel_tools`.

The machine uses the LSF job submission system, so to submit a job you use `bsub < job.lsf`, to check status of jobs you use `bjobs`, and to kill a job use `bkill <jobid>`.

All job scripts must contain a wall time (specified with `-W hr:min`). To submit jobs use `-q scafellpikeSKL -U intel_skl@scafell-pike`. To use these in a job script, each parameter is on a different line at the head of the script, and would be `#BSUB -q scafellpikeSKL` and `#BSUB -U intel_skl@scafell-pike`.

You can find more information on how to use Scafell Pike and LSF at [ServiceNow](https://stfc.service-now.com/hartreecentre?id=kb_article&sys_id=7320ef621ba9c05072a0cbfe6e4bcbd5).
