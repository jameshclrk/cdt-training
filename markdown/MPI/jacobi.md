# MPI Exercises

## Setup
The initial code for these exercises is in the `examples/MPI`. These exercises guide you through a few simple examples using point-to-point operators, and finally the same Jacobi solver as you were shown for OpenMP, but this time needs to be parallelised using MPI.

To compile this exercices load the appropriate environment with `module load cdt-training/intel_tools`.

## MPI Hello World

In this exercise you will run a simple helloworld code (as seen
in the slides) in order to learn the basic steps to compile a code with MPI
and execute it on Scafell Pike. To start go to the `examples/MPI/MPI-helloworld` directory

First compile the code using Intel compiler and submit a job to run the code on the cluster.

```
$ mpiicc helloworld.c -o helloworld_mpi
$ bsub -q scafellpikeSKL -U intel_skl@scafell-pike -n 2 -W  00:05 -o output -e error "mpirun -np 2 ./helloworld_mpi > 2_hello"
$ bsub -q scafellpikeSKL -U intel_skl@scafell-pike -n 32 -W 00:05 -o output -e error "mpirun -np 32 ./helloworld_mpi > 32_hello"
```

You can look at the progress of the jobs with the `bjobs` command.
Then, when each job finishes, `output` and `error` files will be created in your
directory with the stdout and stderr output streams of the binary execution

- What are the results? (these will be in the files `2_hello` and `32_hello`)

## Ping-Pong

In this exercise you will learn to create a simple ping-pong MPI program by
using point-to-point communication.

Create a new directory in the `examples/MPI` directory and start working on 
your pingpong code. You can use the hello world example as a starting point 
if you wish.

- The code should send a value from node 0 to node 1, then another value from
  node 1 to node 0, and you should print out the value recieved by each node.

Once you've created your code, again compile it using `mpiicc`, and submit a 
job to the cluster.

## Ring Example

In this exercise you will create a simple ring communication MPI program using
point-to-point communication.

Create a new directory in the `examples/MPI` directory and start working on your
ring communcation code. 

- The code should send a value from node 0 to node 1, node 1 to node 2, and so on,
with the last node sending a value back to node 0. Each node should print out the
value it receives, increment it and send it on.

Once you've created yorur code, compiled it using `mpiicc` and submit a job to the 
cluster. Try using different numbers of ranks (up to 64) to check it works correctly.

## Jacobi Example

In this exercise you will use MPI to parallelise a simple Jacobi algorithm implementation. Go to `examples/MPI/MPI-jacobi`.
The code functionality is identical to the code you used during the OpenMP lecture.

- First of all, submit a job to run it serially and save the time and results obtained.

- Replace all the `//TODO: - MPI` comments with MPI code, making sure you create and communicate the halos correctly.

- Compile your code, and submit a job to run it with 2 ranks. Record the results you obtain.

- If you get correct results, update the lsf scripts to get multiple executions
  with different numbers of ranks (1 2 4 8 16 32 64) and use `./scaling.py` to generate
a scalability plot.

- Make sure you still get the same results. Has the execution time improved? What is the speedup?

## Jacobi Example Additional Exercises
In the previous part, we only ran the code with 1/2/4/8/16/32/64 nodes, which meant the input array always divided equally
amongst the ranks. Extend the code to run with any number of ranks (Note: This means for the final MPI\_Gather operation, not
all nodes will have the same amount of data to communicate, look up MPI\_GatherV to perform this operation).

