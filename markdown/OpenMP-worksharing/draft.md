# Worksharing Exercises

## Setup
The initial code for these exercises is in the `examples/OpenMP-worksharing`. These exercises introduce the OpenMP `for` method for parallelism.

To compile this exercices load the appropriate environment with `module load
intel-training/anaconda`

## OpenMP Hello World

In this exercise you will run a simple helloworld code (similar as the one seen
in the slides) in order to learn the basic steps to compile a code with OpenMP
and execute it in Scafell Pike. To start go to the `examples/OpenMP-worksharing/00-helloworld` directory. You have C (posfixed .c) and Fortran (postfixed .F90) versions, use your prefered language.

First compile the code using Intel compiler and with and without the OpenMP
compiler flag. And execute the binaries locally.

```
$ [icc/ifort] -Ofast -fopenmp helloworld.[c/F90] -o helloworld_omp
$ ./helloworld_omp
$ [icc/ifort] -Ofast helloworld.[c/F90] -o helloworld_serial
$ ./helloworld_serial
```

- What are the results? What happens when the OpenMP compiler flag is not
included?

Next, we will run a similar example but with the OpenMP runtime library, but in
this case we will submit the job to a Scafell Pike compute node. This will
guarantee us exclusive access to the requested resources. Have
a look at `helloworld_rl.[c/F90]` and `skylakejob.lsf`, can you guess what the output of the program will be?

```
$ [icc/ifort] -Ofast -fopenmp helloworld_rl.[c/F90] -o helloworld
$ ./helloworld
$ bsub < skylakejob.lsf
$ bsub < knljob.lsf
```

You can look at the progress of the jobs with the `bjobs` command.

Then, when each job finishes, .out and .err files will be created in your
directory with the stdout and stderr output streams of the binary execution

- Can you see the results of the Helloworld program on Skylake and KNL? Are the
  results you were expecting?
- Is it possible to compile the Helloworld with runtime library without the
  -fopnemp flag?


## Memory

In this exercise you will learn the multiple types of memory available in the
system, how to utilize them and the performance differences when using each
type.  

First, go to `examples/OpenMP-worksharing/01-numactl`. It contains the STREAM
benchmark, it is a well known test to compute the memory bandwith of a system.
Execute `make` to compile the benchmark.

Look at the `knljob.lsf` script. The script is divided in you parts, the first one gets the processor information using hwloc-ls, the second executes the STREAM benchmark with increasing number of threads and save the output of the Triad test into `stream_output.txt`. Submit to Scafell Pike the `knljob.lsf` script. When the job is ready:

- Can you understand the KNL architecture and memory layout from the `lscpu` and `hwloc-ls` output?
- Execute the `./scaling.py` script to generate a scalability plot from the
  `strean_output.txt`.

Next, modify `knljob.lsf` by prepending the command `numactl -m 0` or `numactl -m 1` before `./stream ...` and generate a each numactl option. (rename each generated scaling.png's to avoid overwriting the files)

- What numbers do you see?
- Which number of threads gives the best performance?
- What sort of speedup do you see when using MCDRAM?
- Why is the performance slow on 1 thread?


## Jacobi Example

In this exercise you will use OpenMP to parallelise a simple Jacobi algorithm implementation. Go to `examples/OpenMP-worksharing/02-jacobi`.

- First of all, run it serially and save the time and results obtained.

- Replace all the `// ADD OMP PRAGMA` comments with an OpenMP pragma, make sure you set the private/shared variables properly.

- If you get correct results, update the lsf scripts to get multiple executions
  with different number of threads and use `./scaling.py` to generate
a scalability plot.

- Make sure you still get the same results. Have the execution time improved? What is the speedup? Can you generate a scalability plot?

- Try using MCDRAM when running the program on the KNL node, does the
  performance improve? How does it compare to the Skylake?

## Jacobi Example Additional Exercises
Try multiple OpenMP schedulers. We recommend adding the `schedule(runtime)`
OpenMP clause and then set up the `OMP_SCHEDULE` environment variable in the
`knljob.lsf` and `skylake.lsf` scripts.

Change the Thread Placement by using multiple combinations of the `OMP_PLACES`
and `OMP_PROC_BIND`  environment variable in the
`knljob.lsf` and `skylake.lsf` scripts. You can also set up `KMP_AFFINITY` to
verbose to visualize the thread mapping.





